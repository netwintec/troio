﻿using UnityEngine;
using System.Collections;

public class PisanoAttack : MonoBehaviour
{

    public int attackDamage = 10;

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;
    PlayerHealth playerHealth;
    bool playerInRange = false;
    bool oneAttack = false;

    float timer;
    float timeBetweenAttacks = 2f;

    // Use this for initialization
    void Start()
    {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        timer = 2f;
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Fermo");
        muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        calcioB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Pedata/Behind");
        pugnoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzotto/Behind");
        favaB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzo/Behind");
        ruttoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto/Behind");

        if (other.gameObject == fermo)
        {

            playerInRange = true;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = true;
        }

        if (other.gameObject == salta)
        {
            playerInRange = true;
        }

        if (other.gameObject == giu)
        {
            playerInRange = true;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = true;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = true;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = true;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = true;
        }

    }



    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == fermo)
        {

            playerInRange = false;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = false;
        }

        if (other.gameObject == salta)
        {
            playerInRange = false;
        }

        if (other.gameObject == giu)
        {
            playerInRange = false;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = false;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = false;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = false;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = false;
        }
    }


    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange && playerHealth.currentHealth > 0)
        {
            Attack();
        }

    }


    void Attack()
    {
        timer = 0f;

        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
