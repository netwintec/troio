﻿using UnityEngine;
using System.Collections;

public class PisanoHealth : MonoBehaviour {

    public int startingHealth = 1;
    public int currentHealth;

    


    PlayerMove playerMove;


    bool isDead;
    bool damaged;


    void Awake()
    {

        //playerMove = GetComponent<PlayerMove>();
        currentHealth = startingHealth;
    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        //healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        //playerMove.Hit();

    }

    void Death()
    {
        isDead = true;

        this.GetComponent<PisanoMove>().die = true;
        //playerMove.Death();

    }
}
