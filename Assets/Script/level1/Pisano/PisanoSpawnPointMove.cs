﻿using UnityEngine;
using System.Collections;

public class PisanoSpawnPointMove : MonoBehaviour {

    public bool Behind;
    public float distance;
    Transform player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Behind)
        {
            transform.position = new Vector3(player.position.x - (distance), transform.position.y, transform.position.z);
        }
        else
        {
            transform.position = new Vector3(player.position.x + (distance), transform.position.y, transform.position.z);
        }

    }

}
