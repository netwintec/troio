﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PortinaioHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    PortinaioManager portinaioManager;

    void Awake()
    {

        currentHealth = startingHealth;
        portinaioManager = GetComponent<PortinaioManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        portinaioManager.Hit();

    }

    void Death()
    {
        isDead = true;

        portinaioManager.Death();
        //playerMove.Death();

    }
}
