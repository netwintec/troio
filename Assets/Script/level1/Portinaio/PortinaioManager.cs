﻿using UnityEngine;
using System.Collections;

public class PortinaioManager : MonoBehaviour {

    Transform enemy;
    GameObject MocioRotante,PortyBase,PortyColpito,PortyMorto;
    public float speed = 4f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame ;

    float TimeInizio,TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundRuota, SoundVola,SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        //Debug.LogError(transform.childCount);
        MocioRotante = transform.Find("MocioRotante").gameObject;
        PortyBase = transform.Find("PortyRotante").gameObject;
        //PortyColpito = transform.FindChild("PortyColpito").gameObject;
        PortyMorto = transform.Find("PortyMorto").gameObject;
        //muovi = GameObject.Find(name + "/PisanoCammina");

        MocioRotante.SetActive(true);
        PortyBase.SetActive(true);
        //PortyColpito.SetActive(false);
        PortyMorto.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundRuota = GameObject.Find(SoundReproducer.name + "/SoundPortinaioRuota");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundPortinaioVola");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundPortinaioMuore");

        StartCoroutine(Sound());
    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            MocioRotante.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            MocioRotante.GetComponent<Animator>().enabled = true;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:"+pausedGame);
    }

    // Update is called once per frame
    void Update () {

        if (GetComponent<PortinaioHealth>().currentHealth>0)
        {

            


            if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().currentHealth > 0)
            {
                if (pausedGame)
                {
                    //float diff = Time.realtimeSinceStartup - TimeInizio;
                    TimeInizio = Time.realtimeSinceStartup - diff;
                    //Debug.LogError(Time.realtimeSinceStartup+"|"+TimeInizio +"|"+diff+"|"+ (Time.realtimeSinceStartup > (TimeInizio + 4.5f)));
                    Debug.LogError("AA:"+TimeInizio);
                }

                //Debug.LogError(Time.realtimeSinceStartup + "|"+ TimeInizio + "|"+(TimeInizio + 4.5f) + "|"+ (Time.realtimeSinceStartup > (TimeInizio + 4.5f)));
                Debug.LogError("AB:"+TimeInizio+"|"+pausedGame);
                if (isStart && Time.realtimeSinceStartup > (TimeInizio + 4.5f))
                {
                    Debug.LogWarning("A" + isDie);
                    if (!isDie)
                    {
                        MocioRotante.SetActive(false);
                        PortyBase.SetActive(true);
                        //PortyColpito.SetActive(false);
                        PortyMorto.SetActive(false);
                    }
                    Wait = true;
                    isStart = false;
                    TimeInizio = Time.realtimeSinceStartup;
                }

                if (Wait && Time.realtimeSinceStartup > (TimeInizio + 1.5f))
                {
                    Debug.LogWarning("B" + isDie);
                    if (!isDie)
                    {
                        MocioRotante.SetActive(true);
                        PortyBase.SetActive(true);
                        //PortyColpito.SetActive(false);
                        PortyMorto.SetActive(false);

                        StartCoroutine(Sound());
                    }

                    isStart = true;
                    Wait = false;
                    TimeInizio = Time.realtimeSinceStartup;
                }

                if (isHit && Time.realtimeSinceStartup > (TimeInizio2 + 0.5f))
                {
                    Debug.LogWarning("C" + isDie);
                    if (!isDie)
                    {
                        if (Wait)
                        {
                            MocioRotante.SetActive(false);
                            PortyBase.SetActive(true);
                            //PortyColpito.SetActive(false);
                            PortyMorto.SetActive(false);
                        }
                    }

                    isHit = false;
                    //Wait = true;
                    //TimeInizio = Time.realtimeSinceStartup;
                }
            }
            else
            {
                MocioRotante.SetActive(false);
                PortyBase.SetActive(true);
                PortyMorto.SetActive(false);
            }

        }
    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        MocioRotante.SetActive(false);
        PortyBase.SetActive(false);
        PortyMorto.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;

        SoundRuota.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

    IEnumerator Sound()
    {

            yield return new WaitForSeconds(1.167f);
            if (!isDie)
                SoundRuota.GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(1.8f);
            if (!isDie) { 
                SoundRuota.GetComponent<AudioSource>().Pause();
                SoundVola.GetComponent<AudioSource>().Play();
            }
    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {
            MocioRotante.SetActive(false);
            PortyBase.SetActive(true);
            //PortyColpito.SetActive(false);
            PortyMorto.SetActive(false);
            //StartCoroutine(AfterHit());
            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }


}
