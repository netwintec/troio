﻿using UnityEngine;
using System.Collections;

public class PortinaioAttack : MonoBehaviour {

    public int attackDamage = 10;

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject colpo;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;
    PlayerHealth playerHealth;
    bool playerInRange = false;
    float timer;

    // Use this for initialization
    void Start()
    {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

        //fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name+"/Fermo");
        //muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        //giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        //salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        //Debug.LogError(fermo.name+"|"+muovi.name+"|"+giu.name+"|"+salta.name);

        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Piattola"), false);
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Fermo");
        muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");
        colpo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Colpo");

        calcioB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Pedata/Behind");
        pugnoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzotto/Behind");
        favaB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzo/Behind");
        ruttoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto/Behind");

        Debug.LogError("Enter:"+other.gameObject.name);
        //Debug.LogError((other.gameObject == fermo) +"|"+ (other.gameObject == muovi) + "|" + (other.gameObject == giu) + "|" + (other.gameObject == salta) );

        if (other.gameObject == fermo)
        {

            playerInRange = true;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = true;
        }

        if (other.gameObject == salta)
        {
            playerInRange = true;
        }

        if (other.gameObject == giu)
        {
            playerInRange = true;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = true;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = true;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = true;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = true;
        }

    }

    void OnTriggerExit2D(Collider2D other)
    {
        Debug.LogError("Exit:" + other.gameObject.name);

        if (other.gameObject == fermo)
        {

            playerInRange = false;
        }

        if (other.gameObject == colpo)
        {

            playerInRange = false;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = false;
        }

        if (other.gameObject == salta)
        {
            playerInRange = false;
        }

        if (other.gameObject == giu)
        {
            playerInRange = false;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = false;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = false;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = false;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = false;
        }
    }


    void Update()
    {

        timer += Time.deltaTime;

        if (timer >= 1f && playerInRange && playerHealth.currentHealth > 0)
        {
            Attack();
            playerInRange = false;
        }

    }


    void Attack()
    {
        timer = 0f;

        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}

