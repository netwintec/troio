﻿using UnityEngine;
using System.Collections;

public class VecchiaSpawnManager : MonoBehaviour {

    public GameObject enemy;
    public Transform spawnPoint;

    void OnTriggerEnter2D(Collider2D other)
    {
        Spawn();
    }


    void Spawn()
    {
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        Destroy(this.gameObject);
    }
}
