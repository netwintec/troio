﻿using UnityEngine;
using System.Collections;

public class VecchiaMove : MonoBehaviour {

    Transform enemy;
    GameObject attack, muovi, muori;
    public float speed = 4f;

    public bool die = false;
    bool MorteSound = false;

    // Use this for initialization
    void Start()
    {

        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        //Debug.LogError(transform.childCount);
        attack = transform.Find("Vecchiabastona").gameObject;
        muovi = transform.Find("Vecchiacammina").gameObject;
        muori = transform.Find("muorivecchiaccia").gameObject;

        muovi.SetActive(false);
        muori.SetActive(false);
        attack.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
        if (die) {

            if (!MorteSound)
            {
                GetComponent<AudioSource>().Play();
                MorteSound = true;
            }

            muori.SetActive(true);
            attack.SetActive(false);
            muovi.SetActive(false);
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().isKinematic = true;
            //GetComponent<BoxCollider2D>().enabled = new Vector2(GetComponent<BoxCollider2D>().offset.x, 0.41f);
            //GetComponent<BoxCollider2D>().size = new Vector2(GetComponent<BoxCollider2D>().size.x, 0.35f);

            StartCoroutine(Die());

        }
        else
        {
            if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().currentHealth > 0)
            {
                if (enemy.position.x > transform.position.x)
                {
                    transform.rotation = new Quaternion(0, 180, 0, 0);
                }
                else
                {
                    transform.rotation = new Quaternion(0, 0, 0, 0);
                }

                //move towards the player
                transform.position += -transform.right * speed * Time.deltaTime;
            }

            if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().currentHealth <= 0)
            {
               
                muovi.SetActive(true);
                attack.SetActive(false);
                muori.SetActive(false);
            }
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }
}

