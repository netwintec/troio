﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

    public int attackDamage = 10;

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;
    PlayerHealth playerHealth;
    bool playerInRange = false;
    bool oneAttack = false;

    float timer;
    float timeBetweenAttacks = 2f;
    

    // Use this for initialization
    void Start()
    {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        timer = Time.realtimeSinceStartup;
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        Debug.LogError(other.gameObject.tag + "|" + other.gameObject.name);

        if (other.gameObject.tag == "Vecchia")
        {
            Attack(other.gameObject, 1);
        }

        if (other.gameObject.tag == "Pisano")
        {
            Attack(other.gameObject, 0);
        }

        if (other.gameObject.tag == "Boss")
        {
            Attack(other.gameObject, 2);
        }

        if (other.gameObject.tag == "Cicciona")
        {
            Attack(other.gameObject, 3);
        }

        if (other.gameObject.tag == "Vedova")
        {
            Attack(other.gameObject, 4);
        }

        if (other.gameObject.tag == "Polparo")
        {
            Attack(other.gameObject, 5);
        }

        if (other.gameObject.tag == "Padrone")
        {
            Attack(other.gameObject, 6);
        }

        if (other.gameObject.tag == "Squalo")
        {
            Attack(other.gameObject, 7);
        }

        if (other.gameObject.tag == "Idraulico")
        {
            Attack(other.gameObject, 8);
        }
   
        if (other.gameObject.name == "NedoRide")
        {
            Attack(other.gameObject, 9);
        }

        var rutto = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto");
        if (other.gameObject.name == "NedoPuzzo" && rutto.active)
        {
            Attack(other.gameObject, 10);
        }

        if (other.gameObject.tag == "Porchettaro")
        {
            Attack(other.gameObject, 11);
        }
    }

    void Update()
    {
        /*timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange && playerHealth.currentHealth > 0)
        {
            Attack();
        }*/

    }


    void Attack(GameObject collider,int id)
    {

        if (id == 1)
        {
            collider.GetComponent<VecchiaHealth>().TakeDamage(attackDamage);
        }

        if (id == 0)
        {
            collider.GetComponent<PisanoHealth>().TakeDamage(attackDamage);
        }

        if (id == 2)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<PortinaioHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 3)
        {
            collider.GetComponent<CiccionaHealth>().TakeDamage(attackDamage);
            timer = Time.realtimeSinceStartup;
            
        }

        if (id == 4)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<VedovaHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 5)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<PolparoHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 6)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<PadroneHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 7)
        {
            collider.GetComponent<SqualoHealth>().TakeDamage(attackDamage);
            timer = Time.realtimeSinceStartup;
        }

        if (id == 8)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<IdraulicoHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 9)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                GameObject.FindGameObjectWithTag("Nedo").GetComponent<NedoHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 10)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                GameObject.Find(GameObject.FindGameObjectWithTag("Nedo").name + "/NedoPuzzo").SetActive(false);//.GetComponent<NedoHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        if (id == 11)
        {
            if (Time.realtimeSinceStartup > timer + 0.2f)
            {
                collider.GetComponent<PorchettaroHealth>().TakeDamage(attackDamage);
                timer = Time.realtimeSinceStartup;
            }
        }

        /*timer = 0f;

        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }*/
    }
}
