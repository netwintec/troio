﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMove: MonoBehaviour {
	

	public bool grounded = false;
    public bool isEnio = false;
    public float speed = 6f;
    public float jumpSpeed = 6f;

    public GameObject fermo;
    public GameObject muovi;
    public GameObject salto;
    public GameObject giu;
    public GameObject cazzotto;
    public GameObject calcio;
    public GameObject rutto;
    public GameObject fava;
    public GameObject muore;
    public GameObject remo;
    public GameObject special;
    public GameObject colpo;
    public GameObject paura;


    Animator anim;
    int save = 1;
    Rigidbody2D Body;
	Transform transf;
	Vector3 movement;
	float hpoint;
    bool canMove = true;
    bool canMoveGiu = true;
    bool canTurn = false;

    bool isCazzotto = false;
    bool isCalcio = false;
    bool isRutto = false;
    bool isFava = false;
    bool isJump = false;
    bool isWalking = false;
    bool isDie = false;
    bool isHit = false;
    bool startSpecial = false;
    bool isSpecial = false;
    bool isPaura = false;
    bool isFinish = false;

    float TimeCazzotto = 0.34f;
    float TimeCalcio = 0.34f;
    float TimeRutto = 0.42f;
    float TimeFava = 0.5f;
    float TimeJump = 0.59f;
    float TimeHit = 0.15f;
    float TimeStartSpecial = 0.34f;
    float TimeSpecial = 15f;
    float TimePaura = 1.2f;

    float TimeInizio;
    float diff;

    bool pausedGame = false;
    bool cammina = false;
    public bool AcquaBlock = false;

    GameObject SoundReproducer,SoundGame, SoundRemo,SoundBoss;
    // Use this for initialization
    void Awake(){
		
		//anim = GetComponent <Animator> ();
		Body = this.GetComponent<Rigidbody2D> ();
		transf = this.transform;
        fermo.SetActive(true);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        muore.SetActive(false);
        remo.SetActive(false);
        special.SetActive(false);
        colpo.SetActive(false);
        paura.SetActive(false);
        colpo.SetActive(false);

        if (isEnio)
        {
            TimeCazzotto = 0.97f;
            TimeCalcio = 0.29f;
            TimeRutto = 0.52f;
            TimeStartSpecial = 0.25f;
            TimeJump = 0.5f;
        }

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundGame = GameObject.Find(SoundReproducer.name + "/SoundGame");
        SoundRemo = GameObject.Find(SoundReproducer.name + "/SoundRemo");
        SoundBoss = GameObject.Find(SoundReproducer.name + "/SoundBoss");

        SoundGame.GetComponent<AudioSource>().Play();

    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        diff = Time.realtimeSinceStartup - TimeInizio;
        Debug.LogError("Player:"+pausedGame);
    }

    // Update is called once per frame

    void Update()
    {

        if (!isDie && !isFinish)
        {
            if (pausedGame)
            {
                //float diff = Time.realtimeSinceStartup - TimeInizio;
                TimeInizio = Time.realtimeSinceStartup - diff;
                //Debug.LogError(Time.realtimeSinceStartup+"|"+TimeInizio +"|"+diff+"|"+ (Time.realtimeSinceStartup > (TimeInizio + 4.5f)));
                Debug.LogError("AA:" + TimeInizio);
            }
        }

    }


    void FixedUpdate(){

        
        //Debug.LogError(pausedGame);
        //Debug.Log(TimeInizio+"   "+(TimeInizio + TimeCazzotto)+ "    "+ (Time.realtimeSinceStartup > TimeInizio + TimeCazzotto));
        if (!isDie && !isFinish)
        {

            if (isCazzotto && Time.realtimeSinceStartup > TimeInizio + TimeCazzotto)
            {
                if(isEnio)
                    GameObject.Find(SoundReproducer.name + "/SoundPugno").GetComponent<AudioSource>().Pause();

                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                isCazzotto = false;
                canMove = true;
                canTurn = false;
            }

            if (isCalcio && Time.realtimeSinceStartup > TimeInizio + TimeCalcio)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                isCalcio = false;
                canMove = true;
                canTurn = false;
            }

            if (isRutto && Time.realtimeSinceStartup > TimeInizio + TimeRutto)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                isRutto = false;
                canMove = true;
                canTurn = false;
            }

            if (isFava && Time.realtimeSinceStartup > TimeInizio + TimeFava)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                isFava = false;
                canMove = true;
                canTurn = false;
            }

            if (isJump && Time.realtimeSinceStartup > TimeInizio + TimeJump)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                isJump = false;
            }

            if (isHit && Time.realtimeSinceStartup > TimeInizio + TimeHit)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);

                canMove = true;
                canTurn = false;
                isHit = false;
            }

            if (startSpecial && Time.realtimeSinceStartup > TimeInizio + TimeStartSpecial)
            {
                special.SetActive(true);
                remo.SetActive(false);

                Destroy(GameObject.FindGameObjectWithTag("Remo"));

                TimeInizio = Time.realtimeSinceStartup;
                canTurn = true;
                startSpecial = false;
                isSpecial = true;
            }

            if (isSpecial && Time.realtimeSinceStartup > TimeInizio + TimeSpecial)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);
                special.SetActive(false);

                SoundGame.GetComponent<AudioSource>().Play();
                SoundRemo.GetComponent<AudioSource>().Pause();
                SoundBoss.GetComponent<AudioSource>().Pause();

                try
                {
                    GameObject.FindGameObjectWithTag("PiattolaManager").GetComponent<PiattolaSpawn>().isAtBoss = false;
                }catch(Exception e)
                { }

                try
                {
                    GameObject.FindGameObjectWithTag("PiattolaManager").GetComponent<PiattoleETopiSpawn>().isAtBoss = false;
                }
                catch (Exception e)
                { }


                canMove = true;
                canTurn = false;
                isSpecial = false;
            }

            if (isPaura && Time.realtimeSinceStartup > TimeInizio + TimePaura)
            {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                colpo.SetActive(false);
                special.SetActive(false);
                paura.SetActive(false);

                canMove = true;
                canTurn = false;
                isPaura = false;
            }
        }

#if !UNITY_ANDROID && !UNITY_IPHONE //|| UNITY_EDITOR
        float h = Input.GetAxisRaw ("Horizontal");
        if (Input.GetAxisRaw("Vertical") == 1 )
        {
            Jump();
        }
        if(Input.GetAxisRaw("Vertical") == -1) {
            Giu(1);
        }
        if (Input.GetAxisRaw("Vertical") == 0)
        {
            Giu(-1);
        }


        Move (h);
		if (save == -1 && h == 1) {
			Debug.Log ("avanti");
			Diritto ();
			
		}
		if (save == 1 && h == -1) {
			Debug.Log ("dietro");
			Indietro ();
			
		}
		Animating (h);
#else

        Move(hpoint);
        if (grounded && isWalking && canMove && !isDie && !isFinish && !pausedGame)
        {
            Animating(hpoint);
            ControlloPosizione();
        }

#endif
    }

    public void ControlloPosizione(){

		if (save == 1) {
			Debug.Log ("avanti");
			Diritto ();
			
		}
		if (save == -1) {
			Debug.Log("dietro");
			Indietro ();
			
		}
	}

	public void StartMoving (float h){
        
		hpoint = h;
	}



	public void Move(float h){

        //Debug.LogWarning(canMove+"   "+ canMoveGiu + "   "+ (canMove && canMoveGiu));
        if (canMove && canMoveGiu && !isDie && !isFinish && !pausedGame)
        {
            movement.Set(h, 0f, 0f);
            movement = movement.normalized * speed * Time.deltaTime;
            transform.position = transform.position + movement;
        }
	}
	
	public void Diritto(){
        if (!pausedGame && !isDie && !isFinish)
        {
            if (canMove)
            {
                save = 1;
                transform.rotation = new Quaternion(0, 0, 0, 0);// RotateAround(transform.position, transform.up, 180f);
            }
            else
            {
                save = 1;
            }

            if (canTurn)
            {
                save = 1;
                transform.rotation = new Quaternion(0, 0, 0, 0);// RotateAround(transform.position, transform.up, 180f);
            }
        }
        //save = 1;
    }
	
	public void Indietro(){
        if (!pausedGame && !isDie && !isFinish)
        {
            if (canMove)
            {
                save = -1;
                transform.rotation = new Quaternion(0, 180, 0, 0);//transform.RotateAround(transform.position, transform.up, 180f);
            }
            else
            {
                save = -1;
            }

            if (canTurn)
            {
                save = -1;
                transform.rotation = new Quaternion(0, 180, 0, 0);//transform.RotateAround(transform.position, transform.up, 180f);
            }
        }
    }
	
	public void Animating(float h){

        isWalking = h != 0f;

        if (canMove && !isJump && !isDie && !isFinish && !pausedGame)
        {

            if (isWalking)
            {
                if (!cammina)
                {
                    GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Play();
                    cammina = true;
                }

                fermo.SetActive(false);
                muovi.SetActive(true);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                muore.SetActive(false);
                remo.SetActive(false);
                special.SetActive(false);
                colpo.SetActive(false);
                paura.SetActive(false);
                colpo.SetActive(false);

            }
            else
            {
                if (cammina)
                {
                    GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();
                    cammina = false;
                }
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                muore.SetActive(false);
                remo.SetActive(false);
                special.SetActive(false);
                colpo.SetActive(false);
                paura.SetActive(false);
                colpo.SetActive(false);

            }
        }
        //anim.SetBool ("isWalking", walking);

    }

    public void  Jump(){
		if (grounded && canMove && canMoveGiu && !isDie && !isFinish && !pausedGame) {

            GameObject.Find(SoundReproducer.name + "/SoundSalto").GetComponent<AudioSource>().Play();
            GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(true);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(false);
            muore.SetActive(false);
            remo.SetActive(false);
            special.SetActive(false);
            colpo.SetActive(false);
            paura.SetActive(false);
            colpo.SetActive(false);

            isJump = true;
            TimeInizio = Time.realtimeSinceStartup;

            Body.velocity = jumpSpeed * Vector2.up;
		}
	}

    public void Giu(int i)
    {
        if (grounded && i==1 && !isDie && !isPaura && !isSpecial && !startSpecial && !isFinish && !pausedGame)
        {
            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(true);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(false);
            muore.SetActive(false);
            remo.SetActive(false);
            special.SetActive(false);
            colpo.SetActive(false);
            paura.SetActive(false);
            colpo.SetActive(false);

            canMoveGiu = false;
        }

        if (grounded && i == -1 && !isDie && !isPaura && !isSpecial && !startSpecial && !isFinish && !pausedGame)
        {
            if (isEnio) {
                fermo.SetActive(true);
                muovi.SetActive(false);
                salto.SetActive(false);
                giu.SetActive(false);
                cazzotto.SetActive(false);
                calcio.SetActive(false);
                rutto.SetActive(false);
                fava.SetActive(false);
                muore.SetActive(false);
                remo.SetActive(false);
                special.SetActive(false);
                colpo.SetActive(false);
                paura.SetActive(false);
                colpo.SetActive(false);

                canMoveGiu = true;
            }
            else { 
                giu.GetComponent<Animator>().SetTrigger("su");
                StartCoroutine(Alzare());
            }
        }
    }

    IEnumerator Alzare()
    {
        giu.GetComponent<Animator>().SetTrigger("su");
        yield return new WaitForSeconds(0.17f);
        fermo.SetActive(true);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        muore.SetActive(false);
        remo.SetActive(false);
        special.SetActive(false);
        colpo.SetActive(false);
        paura.SetActive(false);
        colpo.SetActive(false);

        canMoveGiu = true;
    }

    public void Death()
    {
        isDie = true;

        GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

        fermo.SetActive(false);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        muore.SetActive(true);
        remo.SetActive(false);
        special.SetActive(false);
        colpo.SetActive(false);
        paura.SetActive(false);
        colpo.SetActive(false);

        this.enabled = false;


        StartCoroutine(Die());
    }

    public void Hit()
    {
        isHit = true;
        canMove = false;

        if (isEnio)
            GameObject.Find(SoundReproducer.name + "/SoundPugno").GetComponent<AudioSource>().Pause();

        GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

        fermo.SetActive(false);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        muore.SetActive(false);
        remo.SetActive(false);
        special.SetActive(false);
        colpo.SetActive(false);
        paura.SetActive(false);
        colpo.SetActive(true);

        isCazzotto = false;
        isCalcio = false;
        isRutto = false;
        isFava = false;


        TimeInizio = Time.realtimeSinceStartup;
    }

    public void CazzottoAttack()
    {
        
        if (grounded && canMoveGiu && !isCazzotto && !isCalcio && !isFava && !isRutto && !isDie && !isFinish && !isSpecial && !pausedGame)
        {
            Debug.LogError("Cazzotto");
            //anim.SetTrigger("Cazzotto");

            GameObject.Find(SoundReproducer.name + "/SoundPugno").GetComponent<AudioSource>().Play();
            GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(true);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(false);
            colpo.SetActive(false);

            canMove = false;
            isCazzotto = true;
            TimeInizio = Time.realtimeSinceStartup;
        }

    }

    public void FavaAttack()
    {

        //Debug.LogError(grounded +" "+ canMoveGiu + " " + isCazzotto + " " + isCalcio + " " + isFava + " " + isRutto);
        if (grounded && canMoveGiu && !isCazzotto && !isCalcio && !isFava && !isRutto && !isDie && !isFinish && !isSpecial && !pausedGame &&!AcquaBlock)
        {
            Debug.LogError("Fava");
            //anim.SetTrigger("Favata");

            GameObject.Find(SoundReproducer.name + "/SoundFava").GetComponent<AudioSource>().Play();
            GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(true);
            colpo.SetActive(false);

            canMove = false;
            isFava = true;
            TimeInizio = Time.realtimeSinceStartup;
        }

    }

    public void CalcioAttack()
    {

        if (canMoveGiu && !isCazzotto && !isCalcio && !isFava && !isRutto && !isDie && !isFinish && !isSpecial && !pausedGame && !AcquaBlock)
        {
            Debug.LogError("Calcio");
            //anim.SetTrigger("Calcio");

            GameObject.Find(SoundReproducer.name + "/SoundCalcio").GetComponent<AudioSource>().Play();
            GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(true);
            rutto.SetActive(false);
            fava.SetActive(false);
            colpo.SetActive(false);

            canMove = false;
            isCalcio = true;
            TimeInizio = Time.realtimeSinceStartup;
        }

    }

    public void RuttoAttack()
    {

        if (grounded && canMoveGiu && !isCazzotto && !isCalcio && !isFava && !isRutto && !isDie && !isFinish && !isSpecial && !pausedGame)
        {
            Debug.LogError("Rutto");
            //anim.SetTrigger("Rutto");

            GameObject.Find(SoundReproducer.name + "/SoundRutto").GetComponent<AudioSource>().Play();
            GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(true);
            fava.SetActive(false);
            colpo.SetActive(false);

            canMove = false;
            isRutto = true;
            TimeInizio = Time.realtimeSinceStartup;
        }

    }

    public void SpecialAttack()
    {

        SoundGame.GetComponent<AudioSource>().Pause();
        SoundRemo.GetComponent<AudioSource>().Play();
        SoundBoss.GetComponent<AudioSource>().Pause();

        //Debug.LogWarning(SoundGame.GetComponent<AudioSource>().isPlaying+"|"+ SoundRemo.GetComponent<AudioSource>().isPlaying + "|" + SoundBoss.GetComponent<AudioSource>().isPlaying);
        GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

        GameObject[] PiattoleInGame = GameObject.FindGameObjectsWithTag("Piattola");
        foreach (GameObject piattola in PiattoleInGame)
        {
            Destroy(piattola);
        }

        try
        {
            GameObject.FindGameObjectWithTag("PiattolaManager").GetComponent<PiattolaSpawn>().isAtBoss = true;
        }
        catch (Exception e) { }

        try
        {
            GameObject.FindGameObjectWithTag("PiattolaManager").GetComponent<PiattoleETopiSpawn>().isAtBoss = true;
        }
        catch (Exception e)
        { }


        if (isEnio)
        {
            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(false);
            colpo.SetActive(false);
            special.SetActive(true);
            remo.SetActive(false);

            Destroy(GameObject.FindGameObjectWithTag("Remo"));

            TimeInizio = Time.realtimeSinceStartup;
            canMove = false;
            canTurn = true;
            startSpecial = false;
            isSpecial = true;
        }
        else
        {
            Debug.LogError("Special");
            //anim.SetTrigger("Rutto");

            fermo.SetActive(false);
            muovi.SetActive(false);
            salto.SetActive(false);
            giu.SetActive(false);
            cazzotto.SetActive(false);
            calcio.SetActive(false);
            rutto.SetActive(false);
            fava.SetActive(false);
            colpo.SetActive(false);
            special.SetActive(false);
            remo.SetActive(true);

            canMove = false;
            canTurn = false;
            startSpecial = true;
            TimeInizio = Time.realtimeSinceStartup;
        }
    }

    public void BossFear()
    {
        SoundGame.GetComponent<AudioSource>().Pause();
        SoundRemo.GetComponent<AudioSource>().Pause();
        SoundBoss.GetComponent<AudioSource>().Play();

        GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

        Debug.LogError("Paura");

        fermo.SetActive(false);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        colpo.SetActive(false);
        special.SetActive(false);
        paura.SetActive(true);

        canMove = false;
        canTurn = false;
        isPaura = true;
        TimeInizio = Time.realtimeSinceStartup;

    }

    public void BossDie()
    {
        SoundGame.GetComponent<AudioSource>().Play();
        SoundRemo.GetComponent<AudioSource>().Pause();
        SoundBoss.GetComponent<AudioSource>().Pause();
    }

    public void Finish()
    {

        isFinish = true;

        GameObject.Find(SoundReproducer.name + "/SoundPassi").GetComponent<AudioSource>().Pause();

        SoundGame.GetComponent<AudioSource>().Pause();
        SoundRemo.GetComponent<AudioSource>().Pause();
        SoundBoss.GetComponent<AudioSource>().Pause();

        fermo.SetActive(true);
        muovi.SetActive(false);
        salto.SetActive(false);
        giu.SetActive(false);
        cazzotto.SetActive(false);
        calcio.SetActive(false);
        rutto.SetActive(false);
        fava.SetActive(false);
        muore.SetActive(false);
        remo.SetActive(false);
        special.SetActive(false);
        colpo.SetActive(false);
        paura.SetActive(false);
        colpo.SetActive(false);

        this.enabled = false;
    }

    IEnumerator  Die()
    {
        GameObject.Find(SoundReproducer.name + "/SoundMorte").GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(2f);

        Application.LoadLevel("MenuMorte");

    }
}
