﻿using UnityEngine;
using System.Collections;

public class GroundCheck : MonoBehaviour {

	private PlayerMove player;
	// Use this for initialization
	void Start () {
		player = gameObject.GetComponentInParent<PlayerMove>();
	}

	void OnTriggerEnter2D(Collider2D col){
		player.grounded = true;
        
	}

	void OnTriggerStay2D(Collider2D col){
		player.grounded = true;
	}

	void OnTriggerExit2D(Collider2D col){
		player.grounded = false;
	}
}
