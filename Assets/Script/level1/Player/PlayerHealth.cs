﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    public GameObject Life;
    public Sprite DeathImage;


    PlayerMove playerMove;
    Slider healthSlider;

    bool isDead;
    bool damaged;


    void Awake()
    {

        playerMove = GetComponent<PlayerMove>();
        currentHealth = startingHealth;
        healthSlider = GameObject.Find(Life.name + "/HealthSlider").GetComponent<Slider>();
    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

       //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        playerMove.Hit();

    }

    void Death()
    {
        isDead = true;

        GameObject.Find(Life.name + "/Hearth").GetComponent<Image>().sprite = DeathImage;

        playerMove.Death();

    }

    public void RipristinoVita()
    {
        currentHealth = startingHealth;
        healthSlider.value = currentHealth;
    }


}
