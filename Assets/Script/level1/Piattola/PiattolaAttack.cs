﻿using UnityEngine;
using System.Collections;

public class PiattolaAttack : MonoBehaviour {


    public int attackDamage = 10;

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;
    PlayerHealth playerHealth;
    bool playerInRange = false;
    bool oneAttack = false;

    // Use this for initialization
    void Start()
    {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

        //fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name+"/Fermo");
        //muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        //giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        //salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        //Debug.LogError(fermo.name+"|"+muovi.name+"|"+giu.name+"|"+salta.name);

        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Piattola"), false);
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Fermo");
        muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        calcioB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Pedata/Behind");
        pugnoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzotto/Behind");
        favaB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzo/Behind");
        ruttoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto/Behind");

        //Debug.LogError("Enter:"+other.gameObject.name);
        //Debug.LogError((other.gameObject == fermo) +"|"+ (other.gameObject == muovi) + "|" + (other.gameObject == giu) + "|" + (other.gameObject == salta) );

        if (other.gameObject == fermo)
        {

            playerInRange = true;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = true;
        }

        if (other.gameObject == salta)
        {
            playerInRange = true;
        }

        if (other.gameObject == giu)
        {
            playerInRange = true;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = true;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = true;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = true;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = true;
        }

    }



    void Update()
    {



        if (playerInRange && !oneAttack)
        {
            //Debug.LogError("Attack");
            oneAttack = true;
            Attack();
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Piattola"), true);
        }

        if (oneAttack && transform.position.x < (GameObject.FindGameObjectWithTag("Player").transform.position.x - 0.5))
        {
            Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Piattola"), false);
        }

    }


    void Attack()
    {


        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
