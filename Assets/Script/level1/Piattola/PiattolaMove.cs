﻿using UnityEngine;
using System.Collections;

public class PiattolaMove : MonoBehaviour {

    public float speed = 9f;

    PlayerHealth playerHealth;

    Vector3 movement;

    // Use this for initialization
    void Start()
    {

        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();

    }

    // Update is called once per frame
    void Update()
    {

        if (playerHealth.currentHealth > 0)
        {
            movement.Set(-1f, 0f, 0f);
            movement = movement.normalized * speed * Time.deltaTime;
            transform.position = transform.position + movement;
        }

    }
}
