﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PiattolaSpawn : MonoBehaviour {

    PlayerHealth playerHealth;
    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;
    public bool isAtBoss;

    public Slider FXSound;

    void Start()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        InvokeRepeating("Spawn", spawnTime, spawnTime);

        FXSound.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
    }


    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f )
        {
            return;
        }

        if (isAtBoss)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

        GameObject[] PiattoleInGame2 = GameObject.FindGameObjectsWithTag("Piattola");
        foreach (GameObject piattola2 in PiattoleInGame2)
        {
            piattola2.GetComponent<AudioSource>().volume = FXSound.value;
        }
    }

    public void ValueChangeCheck()
    {
        //Debug.LogWarning(FXSound.value);

        GameObject[] PiattoleInGame = GameObject.FindGameObjectsWithTag("Piattola");
        foreach (GameObject piattola in PiattoleInGame)
        {
            piattola.GetComponent<AudioSource>().volume = FXSound.value;
        }

        GameObject[] PisaniInGame = GameObject.FindGameObjectsWithTag("Pisano");
        foreach (GameObject pisano in PisaniInGame)
        {
            pisano.GetComponent<AudioSource>().volume = FXSound.value;
        }
       
        GameObject[] VecchietteInGame = GameObject.FindGameObjectsWithTag("Vecchia");
        foreach (GameObject vecchie in VecchietteInGame)
        {
            vecchie.GetComponent<AudioSource>().volume = FXSound.value;
        }

        GameObject[] CiccioneInGame = GameObject.FindGameObjectsWithTag("Cicciona");
        foreach (GameObject ciccione in CiccioneInGame)
        {
            ciccione.GetComponent<AudioSource>().volume = FXSound.value;
        }

    }

}
