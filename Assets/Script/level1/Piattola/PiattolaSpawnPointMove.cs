﻿using UnityEngine;
using System.Collections;

public class PiattolaSpawnPointMove : MonoBehaviour {

    Transform player;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = new Vector3(player.position.x + 30,transform.position.y, transform.position.z);

	}

}
