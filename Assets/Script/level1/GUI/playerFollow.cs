﻿using UnityEngine;
using System.Collections;

public class playerFollow : MonoBehaviour {
	
	public Transform player;        // The transform of the projectile to follow.
	public Transform farLeft;           // The transform representing the left bound of the camera's position.
	public Transform farRight;          // The transform representing the right bound of the camera's position.
    public Transform boundBoss;

    public bool isBoss = false;

    void Start()
    {
        
    }

	void Update () {
        if (!isBoss)
        {
            // Store the position of the camera.
            Vector3 newPosition = transform.position;

            // Set the x value of the stored position to that of the bird.
            newPosition.x = player.position.x;

            // Clamp the x value of the stored position between the left and right bounds.
            newPosition.x = Mathf.Clamp(newPosition.x, farLeft.position.x, farRight.position.x);

            // Set the camera's position to this stored position.
            transform.position = newPosition;
        }
        else
        {
            // Store the position of the camera.
            Vector3 newPosition = transform.position;

            // Set the x value of the stored position to that of the bird.
            newPosition.x = player.position.x;

            // Clamp the x value of the stored position between the left and right bounds.
            newPosition.x = Mathf.Clamp(newPosition.x, boundBoss.position.x, boundBoss.position.x);

            // Set the camera's position to this stored position.
            transform.position = newPosition;
        }
	}

    public void StartBoss()
    {
        isBoss = true;
        //boundBoss.position = bound;
    }

    public void BossDie()
    {
        isBoss = false;
    }
}
