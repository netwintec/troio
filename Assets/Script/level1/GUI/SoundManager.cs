﻿using UnityEngine;
using System.Collections;
using System;

public class SoundManager : MonoBehaviour {

    bool open = false;
    public GameObject SoundView;
    public GameObject Player;
    public bool paused = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CompareSound()
    {
        if (open)
        {
            SoundView.SetActive(false);
            Player.GetComponent<PlayerMove>().onPauseGame(false);

            if (GameObject.FindGameObjectWithTag("Boss") != null)
            {
                GameObject.FindGameObjectWithTag("Boss").GetComponent<PortinaioManager>().onPauseGame(false);
            }
            if (GameObject.FindGameObjectWithTag("Vedova") != null)
            {
                GameObject.FindGameObjectWithTag("Vedova").GetComponent<VedovaManager>().onPauseGame(false);
            }
            if (GameObject.FindGameObjectWithTag("Polparo") != null)
            {
                GameObject.FindGameObjectWithTag("Polparo").GetComponent<PolparoManager>().onPauseGame(false);
            }
            if (GameObject.FindGameObjectWithTag("Padrone") != null)
            {
                GameObject.FindGameObjectWithTag("Padrone").GetComponent<PadroneManager>().onPauseGame(false);
            }
            if (GameObject.FindGameObjectWithTag("Idraulico") != null)
            {
                GameObject.FindGameObjectWithTag("Idraulico").GetComponent<IdraulicoManager>().onPauseGame(false);
            }
            if (GameObject.FindGameObjectWithTag("Nedo") != null)
            {
                GameObject.FindGameObjectWithTag("Nedo").GetComponent<NedoManager>().onPauseGame(false);
            }
            Time.timeScale = 1;
            open = false;
        }
        else
        {
            SoundView.SetActive(true);
            Player.GetComponent<PlayerMove>().onPauseGame(true);
            //Debug.LogError("Exist Boss:" + GameObject.FindGameObjectWithTag("Boss")+"|"+ (GameObject.FindGameObjectWithTag("Boss") != null)+"|"+ (GameObject.FindGameObjectWithTag("Boss").GetComponent<PortinaioManager>()));
            if (GameObject.FindGameObjectWithTag("Boss") != null)
            {
                try {
                    GameObject.FindGameObjectWithTag("Boss").GetComponent<PortinaioManager>().onPauseGame(true);

                }catch(Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }

            if (GameObject.FindGameObjectWithTag("Vedova") != null)
            {
                try
                {
                    GameObject.FindGameObjectWithTag("Vedova").GetComponent<VedovaManager>().onPauseGame(true);
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }


            if (GameObject.FindGameObjectWithTag("Polparo") != null)
            {
                try
                {
                    GameObject.FindGameObjectWithTag("Polparo").GetComponent<PolparoManager>().onPauseGame(true);
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }

            if (GameObject.FindGameObjectWithTag("Padrone") != null)
            {
                try
                {
                    GameObject.FindGameObjectWithTag("Padrone").GetComponent<PadroneManager>().onPauseGame(true);
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }

            if (GameObject.FindGameObjectWithTag("Idraulico") != null)
            {
                try
                {
                    GameObject.FindGameObjectWithTag("Idraulico").GetComponent<IdraulicoManager>().onPauseGame(true);
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }
            if (GameObject.FindGameObjectWithTag("Nedo") != null)
            {
                try
                {
                    GameObject.FindGameObjectWithTag("Nedo").GetComponent<NedoManager>().onPauseGame(true);
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e.Message);
                }
            }

            Time.timeScale = 0;
            paused = true;
            open = true;
        }
    }
}
