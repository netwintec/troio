﻿using UnityEngine;
using System.Collections;

public class ScreenResize : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		Screen.fullScreen = true;

		Debug.Log (Screen.currentResolution.width+"     "+ Screen.currentResolution.height);
	}

}
