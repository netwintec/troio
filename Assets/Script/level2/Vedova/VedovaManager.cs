﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VedovaManager : MonoBehaviour {

    Transform enemy;
    GameObject CastVol,VedVola,VedFerma,VedMorta;
    public float speed = 4f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame ;

    float TimeInizio,TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola,SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        //Debug.LogError(transform.childCount);
        CastVol = transform.Find("Castagnaccio_Lancio").gameObject;
        VedVola = transform.Find("Vedova_Lancio").gameObject;
        VedFerma = transform.Find("Vedova_Ferma").gameObject;
        VedMorta = transform.Find("Vedova_Morta").gameObject;
        //muovi = GameObject.Find(name + "/PisanoCammina");

        CastVol.SetActive(false);
        VedVola.SetActive(true);
        VedFerma.SetActive(false);
        VedMorta.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundVedovaColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundVedovaLancio");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundVedovaMuore");

        //StartCoroutine(Sound());
    }
    
    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            CastVol.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            CastVol.GetComponent<Animator>().enabled = true;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:"+pausedGame);
    }

    // Update is called once per frame
    void Update () {

        if (GetComponent<VedovaHealth>().currentHealth>0)
        {


            /*
             
             StartCoroutine(DentieraLaunch());

                Padrone.GetComponent<Animator>().SetTrigger("launch");

                TimeInizio = Time.realtimeSinceStartup;

             */

            if (GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().currentHealth > 0)
            {
                if (pausedGame)
                {
                    //float diff = Time.realtimeSinceStartup - TimeInizio;
                    TimeInizio = Time.realtimeSinceStartup - diff;
                    //Debug.LogError(Time.realtimeSinceStartup+"|"+TimeInizio +"|"+diff+"|"+ (Time.realtimeSinceStartup > (TimeInizio + 4.5f)));
                    Debug.LogError("AA:"+TimeInizio);
                }

                //Debug.LogError(Time.realtimeSinceStartup + "|"+ TimeInizio + "|"+(TimeInizio + 4.5f) + "|"+ (Time.realtimeSinceStartup > (TimeInizio + 4.5f)));
                Debug.LogError("AB:"+TimeInizio+"|"+pausedGame);
                if (isStart && Time.realtimeSinceStartup > (TimeInizio +2.5f))
                {
                    Debug.LogWarning("A" + isDie);
                    if (!isDie)
                    {
                        CastVol.SetActive(false);
                        VedVola.SetActive(true);
                        //PortyColpito.SetActive(false);
                        VedMorta.SetActive(false);
                    }
                    Wait = true;
                    isStart = false;
                    TimeInizio = Time.realtimeSinceStartup;
                }

                if (Wait && Time.realtimeSinceStartup > (TimeInizio + 1.5f))
                {
                    Debug.LogWarning("B" + isDie);
                    if (!isDie)
                    {

                        StartCoroutine(DentieraLaunch());

                        VedVola.GetComponent<Animator>().SetTrigger("launch");

                        TimeInizio = Time.realtimeSinceStartup;

                        StartCoroutine(Sound());
                    }

                    isStart = true;
                    Wait = false;
                    TimeInizio = Time.realtimeSinceStartup;
                }

                if (isHit && Time.realtimeSinceStartup > (TimeInizio2 + 0.5f))
                {
                    Debug.LogWarning("C" + isDie);
                    if (!isDie)
                    {
                        if (Wait)
                        {
                            CastVol.SetActive(false);
                            VedVola.SetActive(true);
                            //PortyColpito.SetActive(false);
                            VedMorta.SetActive(false);
                        }
                    }

                    isHit = false;
                    //Wait = true;
                    //TimeInizio = Time.realtimeSinceStartup;
                }
            }
            else
            {
                CastVol.SetActive(false);
                VedVola.SetActive(true);
                //PortyColpito.SetActive(false);
                VedMorta.SetActive(false);
            }

        }
    }

    IEnumerator DentieraLaunch()
    {
        //yield return new WaitForSeconds(0.2f);

        CastVol.SetActive(true);
        CastVol.GetComponent<VedovaAttack>().enabled = true;

        yield return new WaitForSeconds(2.5f - 0.2f);

       
        CastVol.SetActive(false);
        CastVol.GetComponent<VedovaAttack>().enabled = false;

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        CastVol.SetActive(false);
        VedVola.SetActive(false);
        //PortyColpito.SetActive(false);
        VedMorta.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

    
    IEnumerator Sound()
    {
        yield return new WaitForSeconds(0.2f);
        SoundVola.GetComponent<AudioSource>().Play();
    }
    
    public void Hit()
    {
        if (!isDie && !isStart)
        {
            //ca.SetActive(false);
            //PortyBase.SetActive(true);
            //PortyColpito.SetActive(false);
            //PortyMorto.SetActive(false);
            //StartCoroutine(AfterHit());

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }


}
