﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CiccionaHealth : MonoBehaviour {

    public int startingHealth = 2;
    public int currentHealth;

    bool isDead;
    bool damaged;


    void Awake()
    {

        currentHealth = startingHealth;
    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        //playerMove.Hit();

    }

    void Death()
    {
        isDead = true;

        this.GetComponent<CiccionaMove>().die = true;
        //playerMove.Death();

    }
}
