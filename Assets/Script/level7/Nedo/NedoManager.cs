﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NedoManager : MonoBehaviour {

    public GameObject NedoRide, NedoCalcio, NedoPugno, NedoAscelle, NedoLancio, NedoMuore, Puzzo, Collana;

    Transform enemy;

    public float speed = 4f;
    public float TimeWait = 2f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame;

    float TimeInizio, TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola, SoundAscella, SoundMuore;


    public int i = 0;

    // Use this for initialization
    void Start()
    {

        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        NedoRide.SetActive(true);
        NedoCalcio.SetActive(false);
        NedoPugno.SetActive(false);
        NedoAscelle.SetActive(false);
        NedoLancio.SetActive(false);
        NedoMuore.SetActive(false);
        Puzzo.SetActive(false);
        Collana.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup - 1.8f;

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundBossColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundBossLancio");
        SoundAscella = GameObject.Find(SoundReproducer.name + "/SoundBossAscella");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundBossMuore");

        StartCoroutine(Attack());

    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            Collana.GetComponent<Animator>().enabled = false;
            Puzzo.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            Collana.GetComponent<Animator>().enabled = true;
            Puzzo.GetComponent<Animator>().enabled = true;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:" + pausedGame);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Attack()
    {
        while (GetComponent<NedoHealth>().currentHealth > 0)
        {

            isStart = false;
            yield return new WaitForSeconds(TimeWait);

            if (GetComponent<NedoHealth>().currentHealth > 0)
            {

                var valR = Random.RandomRange(0, 100);
                if (valR < 50)
                {

                    NedoRide.SetActive(true);
                    NedoCalcio.SetActive(false);
                    NedoPugno.SetActive(false);
                    NedoAscelle.SetActive(false);
                    NedoLancio.SetActive(false);
                    NedoMuore.SetActive(false);
                    Puzzo.SetActive(false);
                    Collana.SetActive(false);

                }
                else
                {
                    Debug.LogWarning(enemy.localPosition.x + "|" + transform.localPosition.x);

                    if (enemy.localPosition.x >= (transform.localPosition.x - 6))
                    {

                        var val = Random.RandomRange(0, 100);
                        if (val < 50)
                        {
                            NedoCalcio.SetActive(false);

                            NedoRide.SetActive(false);
                            NedoCalcio.SetActive(true);
                            NedoPugno.SetActive(false);
                            NedoAscelle.SetActive(false);
                            NedoLancio.SetActive(false);
                            NedoMuore.SetActive(false);
                            Puzzo.SetActive(false);
                            Collana.SetActive(false);
                        }
                        else
                        {
                            NedoPugno.SetActive(false);

                            NedoRide.SetActive(false);
                            NedoCalcio.SetActive(false);
                            NedoPugno.SetActive(true);
                            NedoAscelle.SetActive(false);
                            NedoLancio.SetActive(false);
                            NedoMuore.SetActive(false);
                            Puzzo.SetActive(false);
                            Collana.SetActive(false);
                        }

                    }
                    else
                    {

                        var val = Random.RandomRange(0, 100);
                        if (val < 40)
                        {
                            NedoRide.SetActive(false);
                            NedoCalcio.SetActive(false);
                            NedoPugno.SetActive(false);
                            NedoAscelle.SetActive(true);
                            NedoLancio.SetActive(false);
                            NedoMuore.SetActive(false);
                            Puzzo.SetActive(false);
                            Collana.SetActive(false);

                            StartCoroutine(PuzzoLaunch());
                            NedoAscelle.GetComponent<Animator>().SetTrigger("launch");
                        }
                        else
                        {
                            NedoRide.SetActive(false);
                            NedoCalcio.SetActive(false);
                            NedoPugno.SetActive(false);
                            NedoAscelle.SetActive(false);
                            NedoLancio.SetActive(true);
                            NedoMuore.SetActive(false);
                            Puzzo.SetActive(false);
                            Collana.SetActive(false);

                            StartCoroutine(CollanaLaunch());
                            NedoLancio.GetComponent<Animator>().SetTrigger("launch");
                        }

                    }
                }
                
                

                TimeInizio = Time.realtimeSinceStartup;

            }

        }


    }

    IEnumerator PuzzoLaunch()
    {
        yield return new WaitForSeconds(0.1f);

        Puzzo.SetActive(true);
        Puzzo.GetComponent<NedoAttack>().enabled = true;

        SoundAscella.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.2f);

        Puzzo.SetActive(false);
        Puzzo.GetComponent<NedoAttack>().enabled = true;

    }
    IEnumerator CollanaLaunch()
    {

        yield return new WaitForSeconds(0.1f);

        Collana.SetActive(true);
        Collana.GetComponent<NedoAttack>().enabled = true;

        //yield return new WaitForSeconds(0.1f);
        SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        Collana.SetActive(false);
        Collana.GetComponent<NedoAttack>().enabled = true;

    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        NedoRide.SetActive(false);
        NedoCalcio.SetActive(false);
        NedoPugno.SetActive(false);
        NedoAscelle.SetActive(false);
        NedoLancio.SetActive(false);
        NedoMuore.SetActive(true);
        Puzzo.SetActive(false);
        Collana.SetActive(false);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

}