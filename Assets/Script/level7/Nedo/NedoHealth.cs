﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NedoHealth : MonoBehaviour {

    public int startingHealth = 25;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    NedoManager nedoManager;

    void Awake()
    {

        currentHealth = startingHealth;
        nedoManager = GetComponent<NedoManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        nedoManager.Hit();

    }

    void Death()
    {
        isDead = true;

        nedoManager.Death();
        //playerMove.Death();

    }
}
