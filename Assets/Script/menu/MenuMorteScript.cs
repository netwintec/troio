﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuMorteScript : MonoBehaviour {

    bool isTroio;

    public Image Image;
    public Sprite[] Sprites;

    // Use this for initialization
    void Start () {

        isTroio = SelectPlayerScript.isTroio;

        StartCoroutine(PlayMovie());

    }
	
	// Update is called once per frame
	void Update () {
	


	}

    IEnumerator PlayMovie()

    {

        if(isTroio)
            Handheld.PlayFullScreenMovie("MorteTroio.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
        else
            Handheld.PlayFullScreenMovie("MorteBabbo.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();

        StartCoroutine(CountDown());
        //Debug.Log("Video playback completed.");
        //Application.LoadLevel("SelectPlayerMenu");
        //finish = true;
    }

    IEnumerator CountDown()

    {

        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[0];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[1];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[2];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[3];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[4];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[5];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[6];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[7];
        yield return new WaitForSeconds(1f);
        Image.sprite = Sprites[8];
        yield return new WaitForSeconds(1f);
        PlayerPrefs.SetInt("LvlSave", 1);
        Application.LoadLevel("StartGame");

    }


    public void Continua()
    {

        int save = PlayerPrefs.GetInt("LvlSave");

        switch (save)
        {

            case 1:
                if (isTroio)
                {
                    Application.LoadLevel("Level01_Troio");

                }
                else
                {
                    Application.LoadLevel("Level01_Enio");
                }
                break;
            case 2:
                if (isTroio)
                {
                    Application.LoadLevel("Level02_Troio");

                }
                else
                {
                    Application.LoadLevel("Level02_Enio");
                }
                break;
            case 3:
                if (isTroio)
                {
                    Application.LoadLevel("Level03_Troio");

                }
                else
                {
                    Application.LoadLevel("Level03_Enio");
                }
                break;
            case 4:
                if (isTroio)
                {
                    Application.LoadLevel("Level04_Troio");

                }
                else
                {
                    Application.LoadLevel("Level04_Enio");
                }
                break;
            case 5:
                if (isTroio)
                {
                    Application.LoadLevel("Level05_Troio");

                }
                else
                {
                    Application.LoadLevel("Level05_Enio");
                }
                break;
            case 6:
                if (isTroio)
                {
                    Application.LoadLevel("Level06_Troio");

                }
                else
                {
                    Application.LoadLevel("Level06_Enio");
                }
                break;
            case 7:
                if (isTroio)
                {
                    Application.LoadLevel("Level07_Troio");

                }
                else
                {
                    Application.LoadLevel("Level07_Enio");
                }
                break;
            default:
                if (isTroio)
                {
                    Application.LoadLevel("Level01_Troio");

                }
                else
                {
                    Application.LoadLevel("Level01_Enio");
                }
                break;


        }

        
    }
}
