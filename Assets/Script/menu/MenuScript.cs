﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

    bool isTroio = false;
    bool isEnio = false;
    bool clicked = false;
    bool finish = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if(clicked)
        {
            StartCoroutine(PlayMovie());
            clicked = false;
        }

        if (finish) {

            if (isTroio)
            {
                Application.LoadLevel("Level01_Troio");
            }
            if (isEnio)
            {
                Application.LoadLevel("Level01_Enio");
            }

        }
	
	}

    IEnumerator PlayMovie()

    {
        Handheld.PlayFullScreenMovie("BossLvl1.mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();
        Debug.Log("Video playback completed.");
        finish = true;
    }

	public void StartGame(int i){
        if (i == 0) {
            clicked = true;
            isTroio = true;
            //Application.LoadLevel("Level01_Troio");
        }
        if (i == 1)
        {
            clicked = true;
            isEnio = true;
            //Application.LoadLevel("Level01_Enio");
        }
    }
}
