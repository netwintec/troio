﻿using UnityEngine;
using System.Collections;

public class SelectPlayerScript : MonoBehaviour {

    public static bool isTroio;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Troio()
    {
        isTroio = true;
        StartLvl();
    }

    public void Enio()
    {
        isTroio = false;
        StartLvl();
    }


    public void StartLvl()
    {

        int save = PlayerPrefs.GetInt("LvlSave");

        switch (save)
        {

            case 1:
                Application.LoadLevel("VideoLvlOne");
                break;
            case 2:
                Application.LoadLevel("VideoLvlTwo");
                break;
            case 3:
                Application.LoadLevel("VideoLvlThree");
                break;
            case 4:
                Application.LoadLevel("VideoLvlFour");
                break;
            case 5:
                Application.LoadLevel("VideoLvlFive");
                break;
            case 6:
                Application.LoadLevel("VideoLvlSix");
                break;
            case 7:
                Application.LoadLevel("VideoLvlSeven");
                break;
            default:
                Application.LoadLevel("VideoLvlOne");
                break;


        }

    }
}
