﻿using UnityEngine;
using System.Collections;

public class VideoScript : MonoBehaviour {


    bool finish = false;

    //public MovieTexture video;

	// Use this for initialization
	void Start () {

        /*GetComponent<GUITexture>().texture = video as MovieTexture;
        video.Play();
        GetComponent<AudioSource>().clip = video.audioClip;
        GetComponent<AudioSource>().Play();*/
        //string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "Opening.mp4");

        //Handheld.PlayFullScreenMovie("Opening.mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        
        StartCoroutine(PlayMovie());

    }

    IEnumerator PlayMovie()

    {
        Handheld.PlayFullScreenMovie("Opening.mp4", Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        Debug.Log("Video playback completed.");
        finish = true;
    }
        // Update is called once per frame

    void Update () {
	
        if(finish)
            Application.LoadLevel("VideoIntermezzo");

    }
}
