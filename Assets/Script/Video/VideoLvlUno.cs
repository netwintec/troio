﻿using UnityEngine;
using System.Collections;

public class VideoLvlUno : MonoBehaviour {

    bool isTroio;

    public string video;
    public string LivTroio;
    public string LivEnio;
    public int LvlSave = 0;
       
    // Use this for initialization
    void Start()
    {
        isTroio = SelectPlayerScript.isTroio;

        StartCoroutine(PlayMovie());

    }

    IEnumerator PlayMovie()

    {


        PlayerPrefs.SetInt("LvlSave", LvlSave);

        Handheld.PlayFullScreenMovie(video, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();


        Debug.Log("Video playback completed."+isTroio);
        if (isTroio)
            Application.LoadLevel(LivTroio);
        else
            Application.LoadLevel(LivEnio);
        //finish = true;
    }
    // Update is called once per frame

    void Update()
    {

        

    }
}
