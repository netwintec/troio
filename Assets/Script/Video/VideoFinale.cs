﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoFinale : MonoBehaviour {

    bool isTroio;

    public string video;

    // Use this for initialization
    void Start()
    {
        isTroio = SelectPlayerScript.isTroio;

        StartCoroutine(PlayMovie());

    }

    IEnumerator PlayMovie()

    {


        PlayerPrefs.SetInt("LvlSave", 1);

        Handheld.PlayFullScreenMovie(video, Color.black, FullScreenMovieControlMode.Hidden, FullScreenMovieScalingMode.AspectFit);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        Application.LoadLevel("StartGame");
        //finish = true;
    }
    // Update is called once per frame

    void Update()
    {



    }
}
