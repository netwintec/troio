﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;


public class BossBattleLvl5 : MonoBehaviour {

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;

    GameObject Player;

    public GameObject BossPrefab;
    public GameObject Cornice;
    public Camera mainCamera;
    public GameObject PiattoleETopiSpawner;
    public GameObject PisaniSpawn;
    public GameObject BossBattleLeft, BossBattleRight;
    public GameObject BossLife;
    public GameObject BossBackground;
    public Sprite BossDieImage;

    bool playerInRange = false;
    bool oneAttack = false;
    bool die = true;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Fermo");
        muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        calcioB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Pedata/Behind");
        pugnoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzotto/Behind");
        favaB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzo/Behind");
        ruttoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto/Behind");

        //Debug.LogError(other.gameObject);

        if (other.gameObject == fermo)
        {

            playerInRange = true;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = true;
        }

        if (other.gameObject == salta)
        {
            playerInRange = true;
        }

        if (other.gameObject == giu)
        {
            playerInRange = true;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = true;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = true;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = true;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange && !oneAttack)
        {

            Debug.LogError(GameObject.FindGameObjectsWithTag("Pisano").Length + "|" + GameObject.FindGameObjectsWithTag("Piattola").Length);
            oneAttack = true;

            //Vector3 pos = FarLeft.transform.position;
            //pos.x = Player.transform.position.x + 6;
            //FarLeft.transform.position = pos;

            //pos = FarRight.transform.position;
            //pos.x = Player.transform.position.x + 6;
            //FarRight.transform.position = pos;

            //FarLeft.transform.position = new Vector3( Player.transform.position.x + 6,0,0);
            //FarRight.transform.position = new Vector3(Player.transform.position.x + 6, 0, 0);

            BossBackground.SetActive(true);

            GameObject[] PisaniInGame = GameObject.FindGameObjectsWithTag("Pisano");
            foreach (GameObject pisano in PisaniInGame)
            {
                Destroy(pisano);
            }

            GameObject[] PiattoleInGame = GameObject.FindGameObjectsWithTag("Piattola");
            foreach (GameObject piattola in PiattoleInGame)
            {
                Destroy(piattola);
            }

            GameObject[] VecchietteInGame = GameObject.FindGameObjectsWithTag("Vecchia");
            foreach (GameObject vecchie in VecchietteInGame)
            {
                Destroy(vecchie);
            }

            GameObject[] CiccioneInGame = GameObject.FindGameObjectsWithTag("Cicciona");
            foreach (GameObject vecchie in CiccioneInGame)
            {
                Destroy(vecchie);
            }

            PiattoleETopiSpawner.GetComponent<PiattoleETopiSpawn>().isAtBoss = true;
            PisaniSpawn.GetComponent<PisanoSpawnManager>().isAtBoss = true;

            BossBattleLeft.GetComponent<EdgeCollider2D>().enabled = true;
            BossBattleRight.GetComponent<EdgeCollider2D>().enabled = true;


            Player.GetComponent<PlayerMove>().BossFear();
            StartCoroutine(BossStart());

            var SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
            var SoundRide = GameObject.Find(SoundReproducer.name + "/SoundBossToh");
            SoundRide.GetComponent<AudioSource>().Play();
            //Boss.SetActive(true);
            //camera.GetComponent<playerFollow>().enabled = false;
            //contorno.GetComponent<playerFollow>().enabled = false;
        }
        if (oneAttack)
        {
            if (!die)
            {
                try
                {
                    if (GameObject.FindGameObjectWithTag("Porchettaro").GetComponent<PorchettaroHealth>().currentHealth <= 0)
                    {
                        StartCoroutine(BossDie());
                        die = true;
                    }
                }
                catch (Exception e) { }
            }
        }
    }

    IEnumerator BossDie()
    {
        GameObject.Find(BossLife.name + "/Hearth").GetComponent<Image>().sprite = BossDieImage;

        yield return new WaitForSeconds(2f);

        BossBackground.SetActive(true);

        yield return new WaitForSeconds(0.375f);

        Player.GetComponent<Transform>().localScale = new Vector3(1f, 1f, 1);
        BossBattleLeft.GetComponent<EdgeCollider2D>().enabled = false;
        BossBattleRight.GetComponent<EdgeCollider2D>().enabled = false;

        mainCamera.GetComponent<playerFollow>().BossDie();
        Cornice.GetComponent<playerFollow>().BossDie();
        BossLife.SetActive(false);

        yield return new WaitForSeconds(0.375f);

        BossBackground.SetActive(false);

        Player.GetComponent<PlayerMove>().BossDie();

        //FarLeft.transform.position = new Vector3(-217f, 0, 0);
        //FarRight.transform.position = new Vector3(17.5f, 0, 0);
        Destroy(this.gameObject);
    }

    IEnumerator BossStart()
    {
        yield return new WaitForSeconds(0.375f);



        //Debug.LogError(Player.transform.position.x);

        mainCamera.GetComponent<playerFollow>().StartBoss();
        Cornice.GetComponent<playerFollow>().StartBoss();
        BossLife.SetActive(true);
        if (Player.GetComponent<PlayerMove>().isEnio)
        {
            Player.GetComponent<Transform>().localScale = new Vector3(0.9f, 0.9f, 1);
        }
        else
        {
            Player.GetComponent<Transform>().localScale = new Vector3(0.85f, 0.85f, 1);
        }
        GameObject.Instantiate(BossPrefab, new Vector3(255.26f, -0.31f, -3.9f), new Quaternion(0, 0, 0, 0));

        die = false;


        yield return new WaitForSeconds(0.375f);

        BossBackground.SetActive(false);


    }
}
