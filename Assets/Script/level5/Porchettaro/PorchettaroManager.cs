﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PorchettaroManager : MonoBehaviour {

    Transform enemy;
    GameObject PaninoVola, PorchettaroVola, PorchettaroBaffi, PorchettaroMorto;
    public float speed = 4f;
    public float TimeWait = 2f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame;

    float TimeInizio, TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola, SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        //Debug.LogError(transform.childCount);
        PaninoVola = transform.Find("PaninoLancio").gameObject;
        PorchettaroVola = transform.Find("PorchettaroLancio").gameObject;
        PorchettaroBaffi = transform.Find("PorchettaroBaffi").gameObject;
        PorchettaroMorto = transform.Find("PorchettaroMorto").gameObject;
        //muovi = GameObject.Find(name + "/PisanoCammina");

        PaninoVola.SetActive(false);
        PorchettaroVola.SetActive(true);
        PorchettaroBaffi.SetActive(false);
        PorchettaroMorto.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundBossColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundBossLancio");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundBossMuore");

        StartCoroutine(Attack());
    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            PaninoVola.GetComponent<Animator>().enabled = false;
            PorchettaroBaffi.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            PaninoVola.GetComponent<Animator>().enabled = true;
            PorchettaroBaffi.GetComponent<Animator>().enabled = false;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:" + pausedGame);
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Attack()
    {
        while (GetComponent<PorchettaroHealth>().currentHealth > 0)
        {

            isStart = false;
            yield return new WaitForSeconds(TimeWait);

            if (GetComponent<PorchettaroHealth>().currentHealth > 0)
            {
                Debug.LogWarning(enemy.localPosition + "|" + transform.localPosition);
                if (enemy.localPosition.x >= (transform.localPosition.x - 5))
                {

                    PorchettaroBaffi.SetActive(true);
                    PorchettaroVola.SetActive(false);
                    PaninoVola.SetActive(false);

                    PorchettaroBaffi.GetComponent<Animator>().SetTrigger("baffi");

                    StartCoroutine(PorchettaroHit());

                }
                else
                {
                    PorchettaroBaffi.SetActive(false);
                    PorchettaroVola.SetActive(true);
                    PaninoVola.SetActive(false);

                    PorchettaroVola.GetComponent<Animator>().SetTrigger("launch");

                    StartCoroutine(PaninoLaunch());
                }


            }

        }


    }

    IEnumerator PaninoLaunch()
    {
        //yield return new WaitForSeconds(0.2f);

        PaninoVola.SetActive(true);
        PaninoVola.GetComponent<PorchettaroAttack>().enabled = true;

        yield return new WaitForSeconds(0.2f);
        SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        PaninoVola.SetActive(false);
        PaninoVola.GetComponent<PorchettaroAttack>().enabled = true;

    }

    IEnumerator PorchettaroHit()
    {

        PorchettaroBaffi.GetComponent<PorchettaroAttack>().enabled = true;

        yield return new WaitForSeconds(TimeWait - 0.2f);

        PorchettaroBaffi.GetComponent<PorchettaroAttack>().enabled = false;

    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {
            //ca.SetActive(false);
            //PortyBase.SetActive(true);
            //PortyColpito.SetActive(false);
            //PortyMorto.SetActive(false);
            //StartCoroutine(AfterHit());

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        PaninoVola.SetActive(false);
        PorchettaroVola.SetActive(false);
        PorchettaroBaffi.SetActive(false);
        PorchettaroMorto.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

}
