﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PorchettaroHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    PorchettaroManager porchettaroManager;

    void Awake()
    {

        currentHealth = startingHealth;
        porchettaroManager = GetComponent<PorchettaroManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        porchettaroManager.Hit();

    }

    void Death()
    {
        isDead = true;

        porchettaroManager.Death();
        //playerMove.Death();

    }
}
