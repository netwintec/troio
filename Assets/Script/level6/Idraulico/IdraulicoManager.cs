﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdraulicoManager : MonoBehaviour {

    Transform enemy;
    GameObject ChiaveVol, FatturaVol, IdraulicoVola, IdraulicoMorto;
    public float speed = 4f;
    public float TimeWait = 2f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame;

    float TimeInizio, TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola, SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        ChiaveVol = transform.Find("ChiaveLancio").gameObject;
        FatturaVol = transform.Find("FatturaLancio").gameObject;
        IdraulicoVola = transform.Find("IdraulicoLancio").gameObject;
        IdraulicoMorto = transform.Find("IdraulicoMuore").gameObject;

        ChiaveVol.SetActive(false);
        FatturaVol.SetActive(false);
        IdraulicoVola.SetActive(true);
        IdraulicoMorto.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundBossColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundBossLancio");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundBossMuore");

        StartCoroutine(Attack());
    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            ChiaveVol.GetComponent<Animator>().enabled = false;
            FatturaVol.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            ChiaveVol.GetComponent<Animator>().enabled = true;
            FatturaVol.GetComponent<Animator>().enabled = true;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:" + pausedGame);
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Attack()
    {
        while (GetComponent<IdraulicoHealth>().currentHealth > 0)
        {

            isStart = false;
            yield return new WaitForSeconds(TimeWait);

            if (GetComponent<IdraulicoHealth>().currentHealth > 0)
            {

                IdraulicoVola.SetActive(true);
                FatturaVol.SetActive(false);
                ChiaveVol.SetActive(false);
                IdraulicoMorto.SetActive(false);

                var val = Random.RandomRange(0, 6);
                if (val < 5)
                {
                    StartCoroutine(ChiaveLaunch());
                }
                else
                {
                    StartCoroutine(FatturaLaunch());
                }

                IdraulicoVola.GetComponent<Animator>().SetTrigger("launch");

                TimeInizio = Time.realtimeSinceStartup;



            }

        }


    }

    IEnumerator FatturaLaunch()
    {

        yield return new WaitForSeconds(0.2f);

        FatturaVol.SetActive(true);
        FatturaVol.GetComponent<IdraulicoAttack>().enabled = true;

        //yield return new WaitForSeconds(0.1f);
        //SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        FatturaVol.SetActive(false);
        FatturaVol.GetComponent<IdraulicoAttack>().enabled = true;

    }

    IEnumerator ChiaveLaunch()
    {
        yield return new WaitForSeconds(0.2f);

        ChiaveVol.SetActive(true);
        ChiaveVol.GetComponent<IdraulicoAttack>().enabled = true;

        //yield return new WaitForSeconds(0.1f);
        SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        ChiaveVol.SetActive(false);
        ChiaveVol.GetComponent<IdraulicoAttack>().enabled = true;

    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        ChiaveVol.SetActive(false);
        FatturaVol.SetActive(false);
        IdraulicoVola.SetActive(false);
        IdraulicoMorto.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }
}
