﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdraulicoHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    IdraulicoManager idraulicoManager;

    void Awake()
    {

        currentHealth = startingHealth;
        idraulicoManager = GetComponent<IdraulicoManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        idraulicoManager.Hit();

    }

    void Death()
    {
        isDead = true;

        idraulicoManager.Death();
        //playerMove.Death();

    }
}
