﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcquaScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        var player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>();
        player.speed /= 2;
        player.AcquaBlock = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
