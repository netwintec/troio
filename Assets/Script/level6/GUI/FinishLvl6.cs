﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLvl6 : MonoBehaviour {


    // Use this for initialization

    GameObject fermo;
    GameObject muovi;
    GameObject salta;
    GameObject giu;
    GameObject calcioB;
    GameObject pugnoB;
    GameObject favaB;
    GameObject ruttoB;

    GameObject Player;

    public GameObject Finish;

    bool playerInRange = false;
    bool oneAttack = false;

    public string NewLevel;

    void Start()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {

        fermo = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Fermo");
        muovi = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cammina");
        giu = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/SiChina");
        salta = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Salto");

        calcioB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Pedata/Behind");
        pugnoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzotto/Behind");
        favaB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Cazzo/Behind");
        ruttoB = GameObject.Find(GameObject.FindGameObjectWithTag("Player").name + "/Rutto/Behind");

        //Debug.LogError(other.gameObject);

        if (other.gameObject == fermo)
        {

            playerInRange = true;
        }

        if (other.gameObject == muovi)
        {
            playerInRange = true;
        }

        if (other.gameObject == salta)
        {
            playerInRange = true;
        }

        if (other.gameObject == giu)
        {
            playerInRange = true;
        }

        if (other.gameObject == calcioB)
        {

            playerInRange = true;
        }

        if (other.gameObject == pugnoB)
        {
            playerInRange = true;
        }

        if (other.gameObject == favaB)
        {
            playerInRange = true;
        }

        if (other.gameObject == ruttoB)
        {
            playerInRange = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange && !oneAttack)
        {

            Debug.LogError(GameObject.FindGameObjectsWithTag("Pisano").Length + "|" + GameObject.FindGameObjectsWithTag("Piattola").Length);
            oneAttack = true;
            //Finish.SetActive(true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>().Finish();
            GameObject.FindGameObjectWithTag("Player").SetActive(true);//.GetComponent<PlayerMove>().Finish();
            

            var SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
            var SoundVittoria = GameObject.Find(SoundReproducer.name + "/SoundVittoria");
            SoundVittoria.GetComponent<AudioSource>().Play();

            Finish.SetActive(true);

            StartCoroutine(Fine());

        }
    }


    IEnumerator Fine()
    {
        yield return new WaitForSeconds(3.9f);

        var SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        var SoundVittoria = GameObject.Find(SoundReproducer.name + "/SoundVittoria");
        SoundVittoria.GetComponent<AudioSource>().Pause();

        yield return new WaitForSeconds(0.3f);

        if (!string.IsNullOrEmpty(NewLevel))
            Application.LoadLevelAsync(NewLevel);
        //Destroy(GameObject.FindGameObjectWithTag("PlayerFine"));
    }
}


