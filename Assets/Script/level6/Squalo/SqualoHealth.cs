﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SqualoHealth : MonoBehaviour {

    public int startingHealth = 1;
    public int currentHealth;

    public GameObject squalo;


    PlayerMove playerMove;


    bool isDead;
    bool damaged;


    void Awake()
    {

        //playerMove = GetComponent<PlayerMove>();
        currentHealth = startingHealth;
    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        //healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        //playerMove.Hit();

    }

    void Death()
    {
        isDead = true;

        squalo.GetComponent<SqualoMove>().die = true;
        //playerMove.Death();

    }
}
