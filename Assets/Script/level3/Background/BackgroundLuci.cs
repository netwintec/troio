﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundLuci : MonoBehaviour {
    bool Animating = false;
    public bool isAtBoss = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (!isAtBoss && !Animating) {

            var number = Random.Range(1, 1000);
            //Debug.LogWarning(number);
            if (number == 7)
            {
                Animating = true;

                GetComponent<Animator>().SetTrigger("background");
                StartCoroutine(StartCountDown());

            }
        }
		
	}

    IEnumerator StartCountDown()
    {
        yield return new WaitForSeconds(2);

        Animating = false;

    }
}
