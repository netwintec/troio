﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PolparoHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    PolparoManager polparomanager;

    void Awake()
    {

        currentHealth = startingHealth;
        polparomanager = GetComponent<PolparoManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
        /*if (damaged)
        {
            PlayerMove.Hit();
        }
        else
        {
            //damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        damaged = false;*/
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        polparomanager.Hit();

    }

    void Death()
    {
        isDead = true;

        polparomanager.Death();
        //playerMove.Death();

    }
}
