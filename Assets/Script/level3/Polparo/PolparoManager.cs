﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolparoManager : MonoBehaviour {

    Transform enemy;
    GameObject PolpVol, PolparoVola, PolparoCalcio, PolparoMorto;
    public float speed = 4f;
    public float TimeWait = 2f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame;

    float TimeInizio, TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola, SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        //Debug.LogError(transform.childCount);
        PolpVol = transform.Find("Polpo_Lancio").gameObject;
        PolparoVola = transform.Find("Polparo_Lancio").gameObject;
        PolparoCalcio = transform.Find("Polparo_Calcio").gameObject;
        PolparoMorto = transform.Find("Polparo_Morto").gameObject;
        //muovi = GameObject.Find(name + "/PisanoCammina");

        PolpVol.SetActive(false);
        PolparoVola.SetActive(true);
        PolparoCalcio.SetActive(false);
        PolparoMorto.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundBossColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundBossLancio");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundBossMuore");

        StartCoroutine(Attack());
    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            PolpVol.GetComponent<Animator>().enabled = false;
            PolparoCalcio.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            PolpVol.GetComponent<Animator>().enabled = true;
            PolparoCalcio.GetComponent<Animator>().enabled = false;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:" + pausedGame);
    }

    // Update is called once per frame
    void Update () {
		
	}

    IEnumerator Attack()
    {
        while (GetComponent<PolparoHealth>().currentHealth > 0) {

            isStart = false;
            yield return new WaitForSeconds(TimeWait);

            if (GetComponent<PolparoHealth>().currentHealth > 0)
            {
                Debug.LogWarning(enemy.localPosition + "|" + transform.localPosition);
                if (enemy.localPosition.x >= (transform.localPosition.x - 1.5))
                {

                    PolparoCalcio.SetActive(true);
                    PolparoVola.SetActive(false);
                    PolpVol.SetActive(false);

                    PolparoCalcio.GetComponent<Animator>().SetTrigger("calcio");

                    StartCoroutine(PolparoHit());

                }
                else
                {
                    PolparoCalcio.SetActive(false);
                    PolparoVola.SetActive(true);
                    PolpVol.SetActive(false);

                    PolparoVola.GetComponent<Animator>().SetTrigger("launch");

                    StartCoroutine(PolpoLaunch());
                }

               
            }

        }

        
    }

    IEnumerator PolpoLaunch()
    {
        //yield return new WaitForSeconds(0.2f);

        PolpVol.SetActive(true);
        PolpVol.GetComponent<PolparoAttack>().enabled = true;

        yield return new WaitForSeconds(0.2f);
        SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        PolpVol.SetActive(false);
        PolpVol.GetComponent<PolparoAttack>().enabled = true;

    }

    IEnumerator PolparoHit()
    {
        
        PolparoCalcio.GetComponent<PolparoAttack>().enabled = true;

        yield return new WaitForSeconds(TimeWait - 0.2f);

        PolparoCalcio.GetComponent<PolparoAttack>().enabled = false;

    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {
            //ca.SetActive(false);
            //PortyBase.SetActive(true);
            //PortyColpito.SetActive(false);
            //PortyMorto.SetActive(false);
            //StartCoroutine(AfterHit());

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        PolpVol.SetActive(false);
        PolparoVola.SetActive(false);
        PolparoCalcio.SetActive(false);
        PolparoMorto.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

}
