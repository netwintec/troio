﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadroneManager : MonoBehaviour {

    Transform enemy;
    GameObject DentVol, PadroneVola, PadroneMorto;
    public float speed = 4f;
    public float TimeWait = 2f;

    public bool isDie = false;
    bool isStart = false;
    bool Wait = false;
    bool isHit = false;
    bool pausedGame;

    float TimeInizio, TimeInizio2;
    float diff;

    GameObject SoundReproducer, SoundDanno, SoundVola, SoundMuore;

    // Use this for initialization
    void Start()
    {
        pausedGame = false;
        Debug.LogWarning("START");
        enemy = GameObject.FindGameObjectWithTag("Player").transform;

        DentVol = transform.Find("Dentiera").gameObject;
        PadroneVola = transform.Find("Padrone").gameObject;
        PadroneMorto = transform.Find("PadroneMorto").gameObject;

        DentVol.SetActive(false);
        PadroneVola.SetActive(true);
        PadroneMorto.SetActive(false);

        TimeInizio = Time.realtimeSinceStartup + 1f;
        isStart = true;

        SoundReproducer = GameObject.FindGameObjectWithTag("SoundReproductor");
        SoundDanno = GameObject.Find(SoundReproducer.name + "/SoundBossColpo");
        SoundVola = GameObject.Find(SoundReproducer.name + "/SoundBossLancio");
        SoundMuore = GameObject.Find(SoundReproducer.name + "/SoundBossMuore");

        StartCoroutine(Attack());
    }

    public void onPauseGame(bool paused)
    {
        pausedGame = paused;
        if (pausedGame)
        {
            DentVol.GetComponent<Animator>().enabled = false;
            diff = Time.realtimeSinceStartup - TimeInizio;
        }
        else
        {
            DentVol.GetComponent<Animator>().enabled = true;
            //diff = Time.realtimeSinceStartup - TimeInizio;
        }
        Debug.LogError("Boss:" + pausedGame);
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Attack()
    {
        while (GetComponent<PadroneHealth>().currentHealth > 0)
        {

            isStart = false;
            yield return new WaitForSeconds(TimeWait);

            if (GetComponent<PadroneHealth>().currentHealth > 0)
            {

                PadroneMorto.SetActive(false);
                PadroneVola.SetActive(true);
                DentVol.SetActive(false);

                PadroneVola.GetComponent<Animator>().SetTrigger("launch");

                StartCoroutine(DentLaunch());



            }

        }


    }

    IEnumerator DentLaunch()
    {
        yield return new WaitForSeconds(0.2f);

        DentVol.SetActive(true);
        DentVol.GetComponent<PadroneAttack>().enabled = true;

        //yield return new WaitForSeconds(0.1f);
        SoundVola.GetComponent<AudioSource>().Play();

        yield return new WaitForSeconds(TimeWait - 0.4f);

        DentVol.SetActive(false);
        DentVol.GetComponent<PadroneAttack>().enabled = true;

    }

    public void Hit()
    {
        if (!isDie && !isStart)
        {

            SoundDanno.GetComponent<AudioSource>().Play();

            isHit = true;
            TimeInizio2 = Time.realtimeSinceStartup;
        }

    }

    public void Death()
    {
        isDie = true;
        Debug.LogWarning("D" + isDie);

        DentVol.SetActive(false);
        PadroneVola.SetActive(false);
        PadroneMorto.SetActive(true);

        this.GetComponent<BoxCollider2D>().enabled = false;
        this.GetComponent<Rigidbody2D>().isKinematic = true;

        SoundDanno.GetComponent<AudioSource>().Pause();
        SoundVola.GetComponent<AudioSource>().Pause();
        SoundMuore.GetComponent<AudioSource>().Play();

        //this.enabled = false;
        StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(2f);

        Destroy(this.gameObject);
    }

}


