﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PadroneHealth : MonoBehaviour {

    public int startingHealth = 10;
    public int currentHealth;
    Slider healthSlider;

    bool isDead = false;
    bool damaged;
    PadroneManager padroneManager;

    void Awake()
    {

        currentHealth = startingHealth;
        padroneManager = GetComponent<PadroneManager>();
        healthSlider = GameObject.FindWithTag("BossLife").GetComponent<Slider>();

    }


    void Update()
    {
    }


    public void TakeDamage(int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        //playerAudio.Play();

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
        else
        {
            Hit();
        }
    }

    void Hit()
    {

        padroneManager.Hit();

    }

    void Death()
    {
        isDead = true;

        padroneManager.Death();
        //playerMove.Death();

    }
}